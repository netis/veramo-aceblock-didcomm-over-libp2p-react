#!/bin/bash

cd /srv/backend
yarn libp2p:start &

sleep 10

yarn start &

cd /srv/frontend

yarn start
