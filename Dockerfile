FROM node:16.18.1-buster
RUN apt update
RUN apt -y upgrade
RUN wget -c https://golang.org/dl/go1.17.linux-amd64.tar.gz -O go.tar.gz
RUN tar xvf go.tar.gz
RUN mv go /usr/local
ENV GOROOT=/usr/local/go
ENV GOPATH=$HOME/work/
ENV PATH=$GOPATH/bin:$GOROOT/bin:$PATH
COPY ./run.sh /srv/run.sh
COPY backend /srv/backend
COPY frontend /srv/frontend
WORKDIR /srv/backend
ENV LIBP2P_API="localhost:7777"
ENV VERAMO_PORT=3002
ENV DID_COMM_PORT=4001
RUN yarn install
RUN yarn libp2p:init
RUN yarn libp2p:build
RUN npm i -g ts-node
WORKDIR /srv/frontend
ENV REACT_APP_LIBP2P_API="localhost:7777"
ENV REACT_APP_VERAMO_PORT=8888
ENV PORT=3001
ENV SKIP_PREFLIGHT_CHECK=true
RUN yarn install
CMD /srv/run.sh
EXPOSE 3001
EXPOSE 4001