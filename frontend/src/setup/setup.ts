import { Buffer } from "buffer";
import io from "socket.io-client";

export let AliasPortMap = new Map();
export interface UserInfo {
  did: string;
  port: number;
}

global.Buffer = global.Buffer || require("buffer").Buffer;
Buffer.from("anything", "base64");

const socket = io("/");

// message on frontend
export interface msg {
  sender: string;
  message: string;
  // sendStatus codes:
  // undefined: no style
  // string is copied to local style of the element (color)
  sendStatus?: string;
}


async function HandleIncomingMessages(cb: (arg0: any) => void) {
  socket.on("message", cb);
}

export { HandleIncomingMessages };
