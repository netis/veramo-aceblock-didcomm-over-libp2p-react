import React from 'react'
import { IService, DIDResolutionResult } from "@veramo/core";
import { EthrDID } from "ethr-did";
import { JsonRpcSigner, Web3Provider } from "@ethersproject/providers";
import './App.css'
import { ethers } from 'ethers';
import { msg, HandleIncomingMessages } from "./setup/setup"
declare let window: any;

export interface User {
  EthereumAccount: any,
  EthereumChainId: any,
  Web3Provider: Web3Provider,
  Signer: JsonRpcSigner
}

let localUser: User;

export interface connectionsResponse {
  did: string;
  connectionId: string;
  ephemeralDid: string;
}

function isConnectionsRequest(object: any): boolean {
  return 'iss' in object;
}

function isAliasResponse(object: any): boolean {
  return 'alias' in object;
}


async function didManagerAddService(
  did: string,
  service: IService,
  provider: Web3Provider,
  signer: JsonRpcSigner
): Promise<any> {
  const ethrDid = new EthrDID({
    identifier: did,
    provider: provider,
    txSigner: signer,
  });

  const attrName = "did/svc/" + service.type;
  const attrValue = service.serviceEndpoint;
  const ttl = 60 * 60 * 24 * 30 * 12 + 1;
  const gasLimit = 4000000;

  const txHash = await ethrDid.setAttribute(
    attrName,
    attrValue,
    ttl,
    undefined,
    {
      gasLimit,
    }
  );
  return txHash;
}

interface ElementDisabled {
  setupDidButton: boolean,
  connectWithInput: boolean,
  connectWithButton: boolean,
  messagesInput: boolean,
}

interface WalletState {
  connected: boolean,
  buttonText: string,
}

interface ConnectedUser {
  alias: string,
  did: string,
  iss: boolean,
}

// key is did.
let connectedUsers = new Map<string, ConnectedUser>();

let messageEnd: HTMLPreElement | null ;

export default class App extends React.Component<{}, {
  did: string,
  message: string,
  messages: msg[],
  myAlias: string,
  receiverDid: string,
  elementDisabled: ElementDisabled,
  walletState: WalletState
  connected: boolean
}> {
  constructor(props: Promise<any>) {
    super(props);
    this.state = {
      did: "",
      message: "",
      messages: [{ sender: "MetaVerse", message: `Welcome to MetaVerse chat.`, }, { sender: "MetaVerse", message: `First, connect your wallet using MetaMask.`, },],
      myAlias: "",
      receiverDid: "",
      elementDisabled: {
        setupDidButton: true,
        connectWithInput: true,
        connectWithButton: true,
        messagesInput: false
      },
      walletState: {
        connected: false,
        buttonText: "Connect Wallet"
      },
      connected: false
    };
    HandleIncomingMessages(async (data) => {
      try {
        console.log("Incoming message:", data);
        if (isAliasResponse(data.data)) {
          let user = connectedUsers.get(String(data.data.did));
          let _msg: msg = { sender: "MetaVerse", message: `Connected with ${data.data.alias}@(${data.data.did})`, }
          
          if(data.data.hello) {
            // we recieved this connection
            console.log("sending back our alias, since we didn't start this connection", user);
            await sendMessage(prepareHelloMessage(this.state.myAlias, this.state.did), this.state.did, data.data.did);
          }
          connectedUsers.set(data.data.did, {
            alias: data.data.alias,
            did: data.data.did,
            iss: (user !== undefined) ? user.iss: false,
          });
          await this.AddMessage(_msg);
        } else if (!isConnectionsRequest(data.data)) {
          // Display the message
          let user = connectedUsers.get(String(data.data.did));
          if(user !== undefined) {
            let _msg: msg = { sender: user.alias, message: JSON.stringify(data.data.message), }
            await this.AddMessage(_msg);
          }
        } else {
          console.log("getting connection requests from", data.data.iss);
          let user = connectedUsers.get(String(data.data.did));
          connectedUsers.set(data.data.iss, {
            alias: "",
            did: data.data.iss,
            iss: true,
          });
          if(user === undefined) {
            // we recieved this connection
            await this.AddMessage({ sender: "MetaVerse", message: "Recieved connection request from did: " + data.data.iss,});
          }
        }
      } catch (error) {
        console.log(error)
      }
    });
  }

  /**
   * ManageWallet enables to connect/disconnect crypto wallet (Metamask)
   */
  ManageWallet = async () => {
    if (this.state.walletState.connected === false) {
      // Connect wallet
      try {

        let _msg: msg = { sender: "MetaVerse", message: `Connecting your wallet.`, }
        await this.AddMessage(_msg);
        localUser = await connectWallet();
        _msg = { sender: "MetaVerse", message: `Your wallet ${localUser.EthereumAccount} is connected.`, }
        await this.AddMessage(_msg);
        _msg = { sender: "MetaVerse", message: `Next, set up your Decentralized Identifier (DID)`, }
        await this.AddMessage(_msg);
        const elementDisabledState = {
          setupDidButton: false,
          connectWithInput: true,
          connectWithButton: true,
          messagesInput: true
        };

        this.setState({ elementDisabled: elementDisabledState, walletState: { connected: true, buttonText: "Disconnect Wallet" } });
      } catch (error) {
        const _msg = { sender: "MetaVerse:ERROR", message: `Failed to connect wallet, reason: ${error}`, sendStatus: 'red' }
        await this.AddMessage(_msg);
      }
    } else {

      // Disconnect wallet (TODO: actually disconnect metamask as well..)
      try {
        // Tell user we'll disconnect the wallet
        let _msg = { sender: "MetaVerse", message: `Disconnecting your wallet.`, }
        await this.AddMessage(_msg);

        // Disconnect the wallet by clearing the address variable and disabling the app elements
        const elementDisabledState = {
          setupDidButton: true,
          connectWithInput: true,
          connectWithButton: true,
          messagesInput: true
        };

        // Update the state
        this.setState({ elementDisabled: elementDisabledState, walletState: { connected: false, buttonText: "Connect Wallet" } });

        // Notify the user that the wallet was successfully disconnected
        _msg = { sender: "MetaVerse", message: `Your wallet is disconnected.`, }
        await this.AddMessage(_msg);
      } catch (error) {
        const _msg = { sender: "MetaVerse:ERROR", message: `Failed to disconnect your wallet. Error message: ${error}`, sendStatus: 'red' }
        await this.AddMessage(_msg);
      }
    }
  }

  /**
   * ManageDID updates the user DID
   */
  ManageDID = async () => {
    try {
      if(this.state.myAlias === "") {
        await this.AddMessage({ sender: "MetaVerse:ERROR", message: "Unable to setup DID, please input your alias first!", sendStatus: 'red'});
        return
      }
      let _msg: msg = { sender: "MetaVerse", message: `Setting up your DID.` }
      await this.AddMessage(_msg);

      const _did = await manageDID(localUser)

      this.setState({ did: _did })
      _msg = { sender: "MetaVerse", message: `Your DID is set up: ${_did}` }
      await this.AddMessage(_msg);
      _msg = { sender: "MetaVerse", message: `Now you can connect with other MetaVerse users.` }
      await this.AddMessage(_msg);

      const elementDisabledState = {
        setupDidButton: true,
        connectWithInput: false,
        connectWithButton: false,
        messagesInput: false,
      };
      this.setState({ elementDisabled: elementDisabledState });
    } catch (error) {
      const _msg = { sender: "MetaVerse:ERROR", message: `Failed to update your DID, reason: ${error}`, sendStatus: 'red' }
      await this.AddMessage(_msg);
    }
  }

  /**
   * EstablishConnection enables to connect with other users.
   * TODO: sign the 1st message with Metamask
   */
  EstablishConnection = async () => {
    // Receiver DID (trimmed, because copy paste can leave a trailing blank space, causing issues with connections)
    const receiverDid = this.state.receiverDid.trim();

    if(connectedUsers.get(receiverDid) !== undefined) {

      let _msg: msg = { sender: "MetaVerse", message: `Already connected with ${receiverDid}, alias:${connectedUsers.get(receiverDid)?.alias}` }
      await this.AddMessage(_msg);
      return
    }

    if(receiverDid === this.state.did) {
      let _msg: msg = { sender: "MetaVerse:ERROR", message: `Cannot connect to yourself!`, sendStatus: 'red' }
      await this.AddMessage(_msg);
      return
    }

    // Notify user
    let _msg: msg = { sender: "MetaVerse", message: `Connecting with ${receiverDid}` }
    await this.AddMessage(_msg);

    try {
      this.setState({receiverDid: ""});
      // remember the connection
      connectedUsers.set(receiverDid, {
        alias: "",
        did: receiverDid,
        iss: true,
      })
      // start connection
      console.log("establish connection recieved:", await connect(this.state.did, receiverDid));
      // send greeting to connection
      await sendMessage(prepareFirstMessage(this.state.myAlias, this.state.did), this.state.did, receiverDid)
      _msg = { sender: "MetaVerse", message: `Sent connection request to ${receiverDid}` }
      await this.AddMessage(_msg);
    } catch (error) {
      connectedUsers.delete(receiverDid);
      let _msg: msg = { sender: "MetaVerse:ERROR", message: `Failed to connect with ${receiverDid}. ${error}`, sendStatus: 'red' }
      await this.AddMessage(_msg);
      return;
    }
  }

  // TODO: we need to add autoscroll to bottom
  // divRef = useRef<null | HTMLPreElement>(null)
  // if (this.divRef && this.divRef.current) {
  //   this.divRef.current.scrollTop = this.divRef.current.scrollHeight
  // }

  // Add message returns callback function, that enables you to change the sendStatus of the message.
  async AddMessage(_msg: msg ): Promise<(ok?: string) => void> {
    if (!_msg.sender) {
      _msg.sender = "me";
    }
    this.state.messages.push(_msg);
    this.setState({ messages: this.state.messages });
    if(messageEnd !== null) {
      messageEnd.scrollTop = messageEnd.scrollHeight;
    }
    return (ok?: string) => {
      _msg.sendStatus = ok;
      this.setState({ messages: this.state.messages });
      if(messageEnd !== null) {
        messageEnd.scrollTop = messageEnd.scrollHeight;
      }
    }
  }

  HandleMessages = async (event: React.SyntheticEvent<HTMLFormElement>) => {
    event.preventDefault()
    const form = event.currentTarget
    let message = prepareMessage((form.elements as typeof form.elements & {
      messages: { value: string }
    }).messages.value, this.state.did);
    this.setState({ message: "" });
    console.log("sending message", message.body.message);
    let _msg: msg = { sender: "me", message: message.body.message, sendStatus: 'grey'};
    let sendConfirm = await this.AddMessage(_msg);
    try {
      for(let [did] of Array.from(connectedUsers.entries())) {
        console.log("sending message to", did, this.state.did);
        await sendMessage(message, this.state.did, did);
      }
      sendConfirm(undefined);
    } catch (error) {
      sendConfirm('red');
      await this.AddMessage({ sender: "MetaVerse:ERROR", message: `Failed to send a message ${error}`, sendStatus: 'red' });
      this.state.elementDisabled.messagesInput = false;
      this.setState({ message: "" });
    }
  }

  render() {
    return (
      <div>
        <pre>
          <div className="title">
            MetaVerse Chat 1.0
          </div>

        </pre>
        <div className="body">
          <button className="button" onClick={this.ManageWallet}>{this.state.walletState.buttonText}</button>
          <button disabled={this.state.elementDisabled.setupDidButton} className="button" onClick={this.ManageDID}>Setup DID</button>
          <br/>  
        </div>
        <div className="body">
          <input id="myAlias"
            disabled={this.state.elementDisabled.setupDidButton}
            onChange={(e) => this.setState({  myAlias: e.target.value })}
            value={this.state.myAlias}
            placeholder='Enter Alias'
            type="text"
          />
        </div>
        <div className="body">
          <pre className='preText'>## Connect with your friend</pre>
          <input id="receiverDid"
            disabled={this.state.elementDisabled.connectWithInput}
            onChange={(e) => this.setState({ receiverDid: e.target.value })}
            value={this.state.receiverDid}
            placeholder='Enter DID'
            type="text"
          />
          <button disabled={this.state.elementDisabled.connectWithButton} className="button" onClick={this.EstablishConnection}>Connect</button>
        </div>

        <div className='body' id="msg-window">
          <pre className='preText'>## Chat </pre>
          <pre id="log" ref={(el) => { messageEnd = el; }}>
            {
              this.state.messages.map((item) => (
                <div className="typewriter" style={(item.sendStatus) ? {color: item.sendStatus}: {}}>&gt; {item.sender}: {item.message} </div>
              ))
            }
          </pre>
          <form onSubmit={this.HandleMessages}>
            <input id="messages"
              disabled={this.state.elementDisabled.messagesInput}
              onChange={(e) => this.setState({ message: e.target.value })}
              value={this.state.message}
              placeholder='Message'
              type="text"
            />
            <br></br>
          </form>
        </div>

        <pre className="footer">
          Powered by <a href="https://aceblock.com/">AceBlock</a> and <a href="https://veramo.io/">Veramo</a>
        </pre>
      </div>
    );
  }
};

// libp2pService is an object tha
export interface libp2pService {
  id: string;
  type: string;
  routingKeys: string[];
  serviceEndpoint: string;
  accept: string[];
}

// ConnectWallet asks the user to connect its Metamask to the website
async function connectWallet(): Promise<User> {

  // Check if MetaMask is installed on user's browser
  try {

    // check if metamask exists
    if (window.ethereum) {
      // Connect Metamask
      const accounts = await window.ethereum.request({
        method: "wallet_requestPermissions",
        params: [{
          eth_accounts: {}
        }]
      }).then(() => window.ethereum.request({
        method: 'eth_requestAccounts'
      }))

      const chainId = await window.ethereum.request({ method: 'eth_chainId' });


      // Check if user is connected to Mainnet
      if (chainId !== '0x5') {
        throw new Error("Please connect to Goerli")
      }
      // Set the local user
      const provider = new ethers.providers.Web3Provider(window.ethereum, "any");
      return {
        EthereumAccount: accounts[0],
        EthereumChainId: chainId,
        Web3Provider: provider,
        Signer: provider.getSigner(0)
      }
    } else {
      throw new Error("Please install MetaMask");
    }
  } catch (error) {
    console.log(error);
    throw error;
  }
}

// manageDID checks if DID is registered, if not, it creates a new one
async function manageDID(user: User): Promise<string> {
  try {
    // 1 Check if DID is registered
    const did: string = `did:ethr:goerli:${user.EthereumAccount}`;
    let didResult: DIDResolutionResult;
    didResult = await fetch(`/veramo/identifiers/v1/${did}`)
      .then(response => {
        if (!response.ok) {
          throw new Error(response.statusText)
        }
        return response.json()
      })
    // 2 Check the DID service endpoint
    // Get libp2p service info to update the service endpoint

    // Obtain the services
    let services: libp2pService[];
    try {
      services = await fetch("/libp2p/v1/services")
        .then(response => {
          if (!response.ok) {
            throw new Error(response.statusText)
          }
          return response.json()
        })
    } catch (error) {
      console.log(error);
      throw error;
    }

    // TODO: Check if this works correctly -- namely, it always updates the DID Document, even if nothing changes
    for (let service of services) {

      let serviceRegistered = false;
      if (didResult.didDocument?.service !== undefined) {
        for (let didService of didResult.didDocument?.service) {
          console.log(didService.serviceEndpoint, service.serviceEndpoint)
          if (didService.serviceEndpoint === service.serviceEndpoint) {
            serviceRegistered = true;
            continue
          }
        }
      }

      // Libp2p service needs to be added
      if (!serviceRegistered) {
        // Check if the service endpoint needs to be updated
        // If DID is not found, create a new one
        await didManagerAddService(
          did,
          {
            id: 'urn:libp2p:network:aceblock:protocol:didcomm/v2',
            type: 'DIDComm',
            serviceEndpoint: service.serviceEndpoint
          },
          user.Web3Provider,
          user.Signer
        )
      }
    }
    return did;

  } catch (error) {
    console.log(error)
    throw (error)
  }
}


export interface identity {
  did: string;
  alias: string;
}

export interface connectionsRequest {
  sender: identity;
  receiver: identity;
}

// connectWithDID establishes a new connection and exchanges the first message that contains
async function connect(senderDid: string, receiverDid: string): Promise<string> {
  try {

    // Message data
    const data = {
      sender: {
        did: senderDid,
        alias: senderDid,
      },
      receiver: {
        did: receiverDid,
        alias: receiverDid,
      }
    }

    // 1. Establish a connection
    let response = await fetch(`/veramo/messaging/v1/users/${senderDid}/connections`, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(data),
    })
    if(response.status !== 200) {
      throw new Error("Status: " + response.statusText + await response.text());
    }
    return response.text()
  } catch (error) {
    console.log(error);
    throw error;
  }

  // 2. Sign the message with metamask

  // 3. Send the signed message
  // const response = await fetch(`/messages/${did}`, {
  //   method: "POST",
  //   headers: { "Content-Type": "application/json" }
  // })

  // POST
  // Send a message with connection DID

  // const data = {

  // }
  // await fetch(`/veramo/messaging/v1/users/${senderDid}/receivers/${receiverDid}/messages`, {
  //   method: "POST",
  //   headers: { "Content-Type": "application/json" },
  //   body: JSON.stringify(data)
  // });

}

export interface Message {
  type: string;
  id: string;
  body: object;
  packing: string;
}

export interface InitMessage {
  type: string;
  id: string;
  body: {
    iss: string;
    aud: string;
    ephemeralDid: string;
  };
  packing: string;
}


function prepareFirstMessage(alias: string, did: string) {
  return {
    type: "DIDComm",
    id: "DIDComm",
    body: { "alias": alias, "did": did, "establishConnection": true},
    packing: "jws"
  }
}

function prepareHelloMessage(alias: string, did: string) {
  return {
    type: "DIDComm",
    id: "DIDComm",
    body: { "alias": alias, "did": did, "establishConnection": false},
    packing: "jws"
  }
}

function prepareMessage(message: string, did: string) {
  return {
    type: "DIDComm",
    id: "DIDComm",
    body: { "message": message, "did": did},
    packing: "jws"
  }
}

// send the message. Message must be prepared with function prepareMessage or prepareHelloMessage!!
async function sendMessage(message: any, senderDid: string, receiverDid: string) {
  try {
    const response = await fetch(`/veramo/messaging/v1/users/${senderDid}/receivers/${receiverDid}/messages`, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(message)
    })
    console.log(response)
    return
  } catch (error) {
    console.log(`ERROR: ${error}`);
    alert(error);
    throw error;
  }
}