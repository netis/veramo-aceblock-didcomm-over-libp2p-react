# MetaVerse Chat - Web3 and SSI in action

MetaVerse Chat is a simple demo to showcase

- how to manage Decentralised Identifiers (DIDs) using a Web3 wallet
- how to establish an authenticated and encrypted P2P communication channel
- how to securely exchange messages using DIDComm

The demo builds on Web3 and SSI technologies, such as [Veramo](https://veramo.io/), [Web3 Wallets](https://metamask.io/), [LibP2P](https://libp2p.io/),
[Decentralised Identifiers](https://www.w3.org/TR/did-core/) (DIDs), and [DIDComm](https://didcomm.org/).

For details of the demo see the [demo overview](./docs/didcomm-libp2p-demo.md).

Notice: The purpose is to demonstrate the proof of concept.

## Getting Started

### Prerequisite

- [yarn](https://yarnpkg.com/)
- [npm](https://www.npmjs.com/)
- ts-node (`npm i -g ts-node`)
- [GO](https://go.dev/dl/)

### Build

To build the demo, run:

```sh
cd frontend
yarn install

cd ../backend
yarn install
yarn libp2p:init
yarn libp2p:build
```

### Run

To start the demo, start the backend services

```sh
cd backend
yarn start
```

In a separate terminal window, run

```sh
cd frontend
yarn start
```

### Running several instances in the same environment

For testing purposes, one can run several instances in the same environment. To do so, you need to change the port numbers in the following files:

```bash
backend/.env
frontend/.env
frontend/package.json # ("proxy" must point to "VERAMO_PORT" set in backend/.env)
```

Also make sure, that there is no client.json in backent/config. (In case, you copy/paste the folder on your local enviroment, and you have ran the first env. before)

### Docker version

For the docker version, you will need to install [Docker](https://docs.docker.com/engine/install/).

For ease of use, we have created a docker image of this project, which you can find here: [Link](https://hub.docker.com/r/aceblock/veramo-libp2p-chat)

Prequirements for using Docker images is that you have docker installed. To start the image, you can simply run it via:

```docker run -dp 3000:3001 -p 4000:4001 aceblock/veramo-libp2p-chat:latest```


If the above command does not work, you might need to pull the image first with:

```docker pull aceblock/veramo-libp2p-chat:latest```

NOTE:
Port 3000:3001 exposes the frontend of the program (your browser points to this port), while 4000:4001 exposes the DIDComm component. If we want to change for example the frontend to be accessible on localhost:5234, we would type:

```docker run -dp 5234:3001 -p 4000:4001 aceblock/veramo-libp2p-chat:latest```

And that's it!
If you want to run multiple instances of this program (to test the chatting functionality), simply run a new docker container with diffrent ports, for example:

```docker run -dp 3001:3001 -p 4001:4001 aceblock/veramo-libp2p-chat:latest```

(We changed ports 3000 and 4000 to the ports of our choice.)
