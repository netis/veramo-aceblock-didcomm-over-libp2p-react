# Veramo-AceBlock didcomm over libp2p demo

<img src="img/veramo.png" alt="drawing" width="200"/>

<img src="img/aceblock.png" alt="drawing" width="200"/>

## Context

This demo allowes you to test and explore DIDComm over libp2p.

Summarise:

- DID method: did:ethr -> rinkeby
- DIDComm: encrypted/unencrypted
- Libp2p: direct vs relayed communication

Notice: this is for demonstration purposes only.

## Build

Clone the repository

Install: Yarn, Go

Run: yarn libp2p:init

Run: yarn libp2p:build

## Setup

Required only if you want to test two instances locally.

Modify ports in the `.env` file

## Run

Describe how to start the app

```bash
yarn didcomm:start
```

## Demo

Alice:

curl -X POST localhost:8888/veramo/messaging/v1/users -d '{"alias": "alice"}' -H "Content-Type: application/json"

top up the account (rinkeby; e.g., 0.1 ETH)

```bash
curl -X POST localhost:8888/veramo/messaging/v1/users -d '{"alias": "alice"}' -H "Content-Type: application/json"
```

Bob:
Same steps as for alice

Exchange DIDs out of band
Alice's DID: did:ethr:rinkeby:0x02a775752d11ceb225b5b1e8ccc8492fb3a50e25d3d144506a3f4fec6bc6b41d37
Bob's DID: did:ethr:rinkeby:0x03d1023345322b87c0f840eb80af4e57dde30b3d4a43d5003594200db0c3033a75

Alice:

Connect with Bob

```bash
curl -X POST localhost:8888/veramo/messaging/v1/users/alice/connections -d '{"alias":"bob", "did":"did:ethr:rinkeby:0x03d1023345322b87c0f840eb80af4e57dde30b3d4a43d5003594200db0c3033a75"}' -H "Content-Type: application/json"
```

Send message

```bash
curl -X POST localhost:8888/veramo/messaging/v1/users/alice/receivers/bob/messages -H "Content-Type: application/json" -d '{"type":"test", "id":"test-jws-success", "body":"HelloWorld!"}'
```

## Endpoints and flow

POST /users/v1
body: alias
Response: DID Document

- create DID
- create peerID + service info (libp2p)

## Flow

1. Create a new local user (POST /users/v1)
2. Obtain the DID/DIDDocument (GET /users/v1/{alias}) (alias is the local user alias)
   body: {"did": did}
3. Exchange DID/DID Doc. out-of-band
4. Connect
5. Send messages

POST /users/v1
