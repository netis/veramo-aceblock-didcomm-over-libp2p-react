// Core interfaces
import {
  createAgent,
  IDIDManager,
  IResolver,
  IDataStore,
  IKeyManager,
  TAgent,
  IMessageHandler,
  IEventListener,
  IAgentContext,
} from "@veramo/core";

// Core identity manager plugin
import { DIDManager } from "@veramo/did-manager";

// Ethr did identity provider
import { EthrDIDProvider } from "@veramo/did-provider-ethr";

// Web did identity provider
import { WebDIDProvider } from "@veramo/did-provider-web";

// Core key manager plugin
import { KeyManager } from "@veramo/key-manager";

import { JwtMessageHandler } from "@veramo/did-jwt";
import { MessageHandler } from "@veramo/message-handler";
import { W3cMessageHandler } from "@veramo/credential-w3c";

// Custom key management system for RN
import { KeyManagementSystem, SecretBox } from "@veramo/kms-local";

// Custom resolvers
import { DIDResolverPlugin } from "@veramo/did-resolver";
import { Resolver } from "did-resolver";
import { getResolver as ethrDidResolver } from "ethr-did-resolver";
import { getResolver as webDidResolver } from "web-did-resolver";

import {
  ISelectiveDisclosure,
  SdrMessageHandler,
  SelectiveDisclosure,
} from "@veramo/selective-disclosure";

import {
  DIDComm,
  DIDCommHttpTransport,
  DIDCommMessageHandler,
  IDIDComm,
} from "@veramo/did-comm";

// Storage plugin using TypeOrm
import {
  Entities,
  KeyStore,
  DIDStore,
  IDataStoreORM,
  DataStore,
  DataStoreORM,
  PrivateKeyStore,
  migrations,
} from "@veramo/data-store";

// TypeORM is installed with `@veramo/data-store`
import { Connection, createConnection } from "typeorm";

import { Socket } from "socket.io";

// This will be the name for the local sqlite database for demo purposes
const DATABASE_FILE = "./.database.sqlite";

// You will need to get a project ID from infura https://www.infura.io (on info@netis.si)
const INFURA_PROJECT_ID = "f37a315ec2144d0f843e8965a5ca0ef4";

// This will be the secret key for the KMS
const KMS_SECRET_KEY =
  "0c319ea7e15e9164c2bd9fb3406b865588a56f5d928da608b641fbcb17af670b";

const dbConnection: Promise<Connection> = createConnection({
  type: "sqlite",
  database: DATABASE_FILE,
  synchronize: false,
  migrations,
  migrationsRun: true,
  logging: ["error", "info", "warn"],
  entities: Entities,
});

export let AliasPortMap = new Map();
export let Addressbook = new Map();
export let socket: Socket;
export interface UserInfo {
  did: string;
  port: number;
}

const DIDCommEventSniffer: IEventListener = {
  eventTypes: ["DIDComm", "validatedMessage", "savedMessage"],
  onEvent: handleMessageEvents,
};

export async function setSocket(_socket: Socket) {
  socket = _socket;
}

/**
 * handleMessageEvents handles incomming DIDComm messages
 */
async function handleMessageEvents(
  event: { type: string; data: any },
  context: IAgentContext<{}>
): Promise<void>{
  console.log(`[handleMessageEvents] Event Type: ${event.type}`);
  if (event.type === "savedMessage") {
    console.log(event);
    socket.emit("message", event.data);
  }
}

export let agent: TAgent<
  IDIDManager &
    IKeyManager &
    IDataStore &
    IDataStoreORM &
    IResolver &
    IDIDComm &
    IMessageHandler
>;

const _getAgent = async (): Promise<boolean> => {
  
  agent = createAgent<
    IDIDManager &
      IKeyManager &
      IDataStore &
      IDataStoreORM &
      IResolver &
      IDIDComm &
      IMessageHandler
  >({
    plugins: [
      new KeyManager({
        store: new KeyStore(dbConnection),
        kms: {
          local: new KeyManagementSystem(
            new PrivateKeyStore(dbConnection, new SecretBox(KMS_SECRET_KEY))
          ),
        },
      }),
      new DIDManager({
        store: new DIDStore(dbConnection),
        defaultProvider: "did:ethr:goerli",
        providers: {
          "did:ethr:goerli": new EthrDIDProvider({
            defaultKms: "local",
            network: "goerli",
            rpcUrl: "https://goerli.infura.io/v3/" + INFURA_PROJECT_ID,
            gas: 4000000,
          }),
          "did:web": new WebDIDProvider({
            defaultKms: "local",
          }),
        },
      }),
      new DIDResolverPlugin({
        resolver: new Resolver({
          ...ethrDidResolver({ infuraProjectId: INFURA_PROJECT_ID }),
          ...webDidResolver(),
        }),
      }),
      new DataStore(dbConnection),
      new DataStoreORM(dbConnection),
      new DIDComm([new DIDCommHttpTransport()]),
      new MessageHandler({
        messageHandlers: [
          new DIDCommMessageHandler(),
          new JwtMessageHandler(),
          new W3cMessageHandler(),
          new SdrMessageHandler(),
        ],
      }),
      DIDCommEventSniffer,
    ],
  });

  return true;
};

_getAgent();
