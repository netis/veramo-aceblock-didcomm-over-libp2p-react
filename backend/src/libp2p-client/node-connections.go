package main

import (
	"context"
	"errors"
	"strconv"
	"strings"

	"github.com/libp2p/go-libp2p-core/peer"
	"github.com/libp2p/go-libp2p-core/peerstore"
	"github.com/libp2p/go-libp2p-core/protocol"
	ma "github.com/multiformats/go-multiaddr"
)

// P2PListenerInfoOutput is output type of ls command
type P2PListenerInfoOutput struct {
	Protocol      string
	ListenAddress string
	TargetAddress string
}

// P2PStreamInfoOutput is output type of streams command
type P2PStreamInfoOutput struct {
	HandlerID     string
	Protocol      string
	OriginAddress string
	TargetAddress string
}

// P2PLsOutput is output type of ls command
type P2PLsOutput struct {
	Listeners []P2PListenerInfoOutput
}

// P2PStreamsOutput is output type of streams command
type P2PStreamsOutput struct {
	Streams []P2PStreamInfoOutput
}

// ListenTo
// Create a libp2p service and forward connections made to <target-address>.
//
// <protocol> specifies the libp2p handler name. It must be prefixed with '` + P2PProtoPrefix + `'.
//
// Example:
//   ipfs p2p listen ` + P2PProtoPrefix + `myproto /ip4/127.0.0.1/tcp/1234
//     - Forward connections to 'myproto' libp2p service to 127.0.0.1:1234
func (n *Node) ListenTo(protoOpt, targetOpt string) error {

	proto := protocol.ID(protoOpt)

	target, err := ma.NewMultiaddr(targetOpt)
	if err != nil {
		return err
	}

	// port cannot be 0 or cannot be "taken"
	if err := checkPort(target); err != nil {
		return err
	}

	// if !allowCustom && !strings.HasPrefix(string(proto), P2PProtoPrefix) {
	// 	return errors.New("protocol name must be within '" + P2PProtoPrefix + "' namespace")
	// }

	_, err = n.ForwardRemote(n.Context(), proto, target, false)
	return err
}

// Connect
// Forward connections to libp2p service
// Forward connections made to <listen-address> to <target-address>.
//
// <protocol> specifies the libp2p protocol name to use for libp2p
// connections and/or handlers. It must be prefixed with '` + P2PProtoPrefix + `'.
//
// Example:
//   ipfs p2p forward ` + P2PProtoPrefix + `myproto /ip4/127.0.0.1/tcp/4567 /p2p/QmPeer
//     - Forward connections to 127.0.0.1:4567 to '` + P2PProtoPrefix + `myproto' service on /p2p/QmPeer

func (n *Node) ConnectTo(protoOpt, listenOpt, targetOpt string, allowCustom bool) error {

	proto := protocol.ID(protoOpt)

	listen, err := ma.NewMultiaddr(listenOpt)
	if err != nil {
		return err
	}

	targets, err := parseIpfsAddr(targetOpt)
	if err != nil {
		return err
	}

	if !allowCustom && !strings.HasPrefix(string(proto), P2PProtoPrefix) {
		return errors.New("protocol name must be within '" + P2PProtoPrefix + "' namespace")
	}

	return forwardLocal(n.Context(), n, n.Peerstore, proto, listen, targets)
}

// forwardLocal forwards local connections to a libp2p service
func forwardLocal(ctx context.Context, n *Node, ps peerstore.Peerstore, proto protocol.ID, bindAddr ma.Multiaddr, addr *peer.AddrInfo) error {
	ps.AddAddrs(addr.ID, addr.Addrs, peerstore.TempAddrTTL)
	// TODO: return some info
	_, err := n.ForwardLocal(ctx, addr.ID, proto, bindAddr)
	return err
}

// ListActiveListeners list all the active libp2p listeners
func (n *Node) listActiveListeners() (*P2PLsOutput, error) {

	output := &P2PLsOutput{}

	n.ListenersLocal.Lock()
	for _, listener := range n.ListenersLocal.Listeners {
		output.Listeners = append(output.Listeners, P2PListenerInfoOutput{
			Protocol:      string(listener.Protocol()),
			ListenAddress: listener.ListenAddress().String(),
			TargetAddress: listener.TargetAddress().String(),
		})
	}
	n.ListenersLocal.Unlock()

	n.ListenersP2P.Lock()
	for _, listener := range n.ListenersP2P.Listeners {
		output.Listeners = append(output.Listeners, P2PListenerInfoOutput{
			Protocol:      string(listener.Protocol()),
			ListenAddress: listener.ListenAddress().String(),
			TargetAddress: listener.TargetAddress().String(),
		})
	}
	n.ListenersP2P.Unlock()
	// return cmds.EmitOnce(res, output)
	// Type: P2PLsOutput{},
	// Encoders: cmds.EncoderMap{
	// 	cmds.Text: cmds.MakeTypedEncoder(func(req *cmds.Request, w io.Writer, out *P2PLsOutput) error {
	// 		headers, _ := req.Options[p2pHeadersOptionName].(bool)
	// 		tw := tabwriter.NewWriter(w, 1, 2, 1, ' ', 0)
	// 		for _, listener := range out.Listeners {
	// 			if headers {
	// 				fmt.Fprintln(tw, "Protocol\tListen Address\tTarget Address")
	// 			}

	// 			fmt.Fprintf(tw, "%s\t%s\t%s\n", listener.Protocol, listener.ListenAddress, listener.TargetAddress)
	// 		}
	// 		tw.Flush()

	// 		return nil
	// 	}),
	// },
	return output, nil
}

// closeListeners stops listening for new connections to forward
// We can close all listeners, mach by protocol name, listen address or target address
func (n *Node) closeListeners(all, pOpt, lOpt, tOpt interface{}) error {
	// var p2pCloseCmd = &cmds.Command{
	// 	Helptext: cmds.HelpText{
	// 		Tagline: "Stop listening for new connections to forward.",
	// 	},
	// 	Options: []cmds.Option{
	// 		cmds.BoolOption(p2pAllOptionName, "a", "Close all listeners."),
	// 		cmds.StringOption(p2pProtocolOptionName, "p", "Match protocol name"),
	// 		cmds.StringOption(p2pListenAddressOptionName, "l", "Match listen address"),
	// 		cmds.StringOption(p2pTargetAddressOptionName, "t", "Match target address"),
	// 	},
	// 	Run: func(req *cmds.Request, res cmds.ResponseEmitter, env cmds.Environment) error {

	closeAll, _ := all.(bool)
	protoOpt, p := pOpt.(string)
	listenOpt, l := lOpt.(string)
	targetOpt, t := tOpt.(string)

	proto := protocol.ID(protoOpt)

	var (
		target, listen ma.Multiaddr
		err            error
	)

	if l {
		listen, err = ma.NewMultiaddr(listenOpt)
		if err != nil {
			return err
		}
	}

	if t {
		target, err = ma.NewMultiaddr(targetOpt)
		if err != nil {
			return err
		}
	}

	if !(closeAll || p || l || t) {
		return errors.New("no matching options given")
	}

	if closeAll && (p || l || t) {
		return errors.New("can't combine --all with other matching options")
	}

	match := func(listener Listener) bool {
		if closeAll {
			return true
		}
		if p && proto != listener.Protocol() {
			return false
		}
		if l && !listen.Equal(listener.ListenAddress()) {
			return false
		}
		if t && !target.Equal(listener.TargetAddress()) {
			return false
		}
		return true
	}

	done := n.ListenersLocal.Close(match)
	done += n.ListenersP2P.Close(match)

	// return cmds.EmitOnce(res, done)
	// },
	// Type: int(0),
	// Encoders: cmds.EncoderMap{
	// 	cmds.Text: cmds.MakeTypedEncoder(func(req *cmds.Request, w io.Writer, out int) error {
	// 		fmt.Fprintf(w, "Closed %d stream(s)\n", out)
	// 		return nil
	// 	}),
	// },
	return nil
}

// StreamClose closes an active p2p stream
func (n *Node) StreamClose(closeAll bool, id string) error {

	//var p2pStreamCloseCmd = &cmds.Command{
	//	Helptext: cmds.HelpText{
	//		Tagline: "Close active p2p stream.",
	//	},
	//	Arguments: []cmds.Argument{
	//		cmds.StringArg("id", false, false, "Stream identifier"),
	//	},
	//	Options: []cmds.Option{
	//		cmds.BoolOption(p2pAllOptionName, "a", "Close all streams."),
	//	},
	//	Run: func(req *cmds.Request, res cmds.ResponseEmitter, env cmds.Environment) error {

	// closeAll, _ := req.Options[p2pAllOptionName].(bool)
	var handlerID uint64
	var err error

	if !closeAll {
		if len(id) == 0 {
			return errors.New("no id specified")
		}

		handlerID, err = strconv.ParseUint(id, 10, 64)
		if err != nil {
			return err
		}
	}

	toClose := make([]*Stream, 0, 1)
	n.Streams.Lock()
	for id, stream := range n.Streams.Streams {
		if !closeAll && handlerID != id {
			continue
		}
		toClose = append(toClose, stream)
		if !closeAll {
			break
		}
	}
	n.Streams.Unlock()

	for _, s := range toClose {
		n.Streams.Reset(s)
	}

	return nil
}

// Deprecated
///////
// Stream
//

// // p2pStreamCmd is the 'ipfs p2p stream' command
// var p2pStreamCmd = &cmds.Command{
// 	Helptext: cmds.HelpText{
// 		Tagline:          "P2P stream management.",
// 		ShortDescription: "Create and manage p2p streams",
// 	},
//
// 	Subcommands: map[string]*cmds.Command{
// 		"ls":    p2pStreamLsCmd,
// 		"close": p2pStreamCloseCmd,
// 	},
// }
//
// var p2pStreamLsCmd = &cmds.Command{
// 	Helptext: cmds.HelpText{
// 		Tagline: "List active p2p streams.",
// 	},
// 	Options: []cmds.Option{
// 		cmds.BoolOption(p2pHeadersOptionName, "v", "Print table headers (ID, Protocol, Local, Remote)."),
// 	},
// 	Run: func(req *cmds.Request, res cmds.ResponseEmitter, env cmds.Environment) error {
// 		n, err := p2pGetNode(env)
// 		if err != nil {
// 			return err
// 		}
//
// 		output := &P2PStreamsOutput{}
//
// 		n.P2P.Streams.Lock()
// 		for id, s := range n.P2P.Streams.Streams {
// 			output.Streams = append(output.Streams, P2PStreamInfoOutput{
// 				HandlerID: strconv.FormatUint(id, 10),
//
// 				Protocol: string(s.Protocol),
//
// 				OriginAddress: s.OriginAddr.String(),
// 				TargetAddress: s.TargetAddr.String(),
// 			})
// 		}
// 		n.P2P.Streams.Unlock()
//
// 		return cmds.EmitOnce(res, output)
// 	},
// 	Type: P2PStreamsOutput{},
// 	Encoders: cmds.EncoderMap{
// 		cmds.Text: cmds.MakeTypedEncoder(func(req *cmds.Request, w io.Writer, out *P2PStreamsOutput) error {
// 			headers, _ := req.Options[p2pHeadersOptionName].(bool)
// 			tw := tabwriter.NewWriter(w, 1, 2, 1, ' ', 0)
// 			for _, stream := range out.Streams {
// 				if headers {
// 					fmt.Fprintln(tw, "ID\tProtocol\tOrigin\tTarget")
// 				}
//
// 				fmt.Fprintf(tw, "%s\t%s\t%s\t%s\n", stream.HandlerID, stream.Protocol, stream.OriginAddress, stream.TargetAddress)
// 			}
// 			tw.Flush()
//
// 			return nil
// 		}),
// 	},
// }
