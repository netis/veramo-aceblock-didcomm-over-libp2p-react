package main

import (
	"context"
	"net/http"

	"github.com/go-kit/kit/endpoint"
)

// GetPeersRequest
// Input arguments for the GetPeers method; Input arguments are not required
type getPeersRequest struct{}

// decodeGetPeersRequest
func decodeGetPeersRequest(_ context.Context, r *http.Request) (interface{}, error) {
	return getPeersRequest{}, nil
}

// GetPeersEndpoint
// Returns a list of connected peers
func GetPeersEndpoint(n *Node) endpoint.Endpoint {
	return func(_ context.Context, request interface{}) (interface{}, error) {
		return n.Host.Network().Peers(), nil
	}
}
