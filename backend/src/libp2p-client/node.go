package main

import (
	"context"
	"strings"
	"time"

	"github.com/hyperledger/aries-framework-go/pkg/doc/did"
	"github.com/sirupsen/logrus"
	log "github.com/sirupsen/logrus"

	"github.com/libp2p/go-libp2p"
	connmgr "github.com/libp2p/go-libp2p-connmgr"
	"github.com/libp2p/go-libp2p-core/host"
	"github.com/libp2p/go-libp2p-core/peer"
	"github.com/libp2p/go-libp2p-core/peerstore"
	"github.com/libp2p/go-libp2p-core/pnet"
	"github.com/libp2p/go-libp2p-core/routing"
	dht "github.com/libp2p/go-libp2p-kad-dht"
	"github.com/libp2p/go-libp2p/config"
)

// Node is a LibP2P node with the following properties
// - ID: libp2p node id
// - Host: libp2p host
// - ListenersLocal: an array of local listeners
// - ListenersP2P: an array of P2P listeners
// - Streams: A registry of open streams
// - BootstrapConfig: Network bootstrap configuration
type Node struct {
	ctx context.Context

	ConfigNetwork ConfigNetwork

	DIDDocument *did.Doc
	ID          peer.ID
	Host        host.Host

	ListenersLocal *Listeners
	ListenersP2P   *Listeners
	Streams        *StreamRegistry
	Peerstore      peerstore.Peerstore
}

// Context returns the IpfsNode context
func (n *Node) Context() context.Context {
	if n.ctx == nil {
		n.ctx = context.TODO()
	}
	return n.ctx
}

// NewLibP2PNode creates a new libp2p node
func NewLibP2PNode(configNode *ConfigNode, configNetwork *ConfigNetwork) (*Node, error) {
	// If secret key doesn't exist, create one and store it to the configuration file

	// Create a host
	// Set your own keypair
	sk, err := configNode.getSecretKey()
	if err != nil {
		panic(err)
	}
	identity, err := peer.IDFromPrivateKey(*sk)
	if err != nil {
		panic(err)
	}
	log.Debug("[NewLibP2PNode] Identity:", identity.String())

	ctx, _ := context.WithCancel(context.Background())
	var idht *dht.IpfsDHT
	newDHT := func(h host.Host) (routing.PeerRouting, error) {
		var err error
		idht, err = dht.New(ctx, h)
		return idht, err
	}
	routing := libp2p.Routing(newDHT)

	logrus.Debug("[NewLibP2pNode] configNetwork.Secret:", configNetwork.Secret)
	psk, err := pnet.DecodeV1PSK(strings.NewReader(strings.TrimRight(configNetwork.Secret, "\n")))
	logrus.Debug(psk, err)
	if err != nil {
		panic(err)
	}

	// Relay nodes
	var relayNodes = []peer.AddrInfo{}
	for _, v := range configNetwork.Bootstrap {
		relayAddress, err := peer.AddrInfoFromString(v)
		if err != nil {
			log.Panic(err)
		}
		relayNodes = append(relayNodes, *relayAddress)
	}

	log.Debug("Relay Nodes:", relayNodes)

	opts := libp2p.ChainOptions(
		// libp2p Default Security
		// libp2p.DefaultSecurity,
		// libp2p.DefaultSecurity,
		// support TLS connections
		libp2p.Security(tlsID, TLSNew),
		// Add noise
		// libp2p.Security(noise.ID, noise.New),
		// Set the node private key
		libp2p.Identity(*sk),
		// Set the node listen addresses

		libp2p.ListenAddrStrings(configNode.Addresses.Swarm...),
		// Unset listeners - for relay-communication testing only
		// libp2p.ListenAddrs(),

		// Enable communication via relay nodes
		libp2p.EnableRelay(),
		// Let this host use relays and advertise itself on relays if
		// it finds it is behind NAT. Use libp2p.Relay(options...) to
		// enable active relays and more.
		libp2p.EnableAutoRelay(),
		//
		// libp2p.StaticRelays(relayNodes),
		// Connect to a private network
		libp2p.PrivateNetwork(psk),
		// Let's prevent our peer from having too many
		// connections by attaching a connection manager.
		libp2p.ConnectionManager(connmgr.NewConnManager(
			100,         // Lowwater
			400,         // HighWater,
			time.Minute, // GracePeriod
		)),
		// Let this host use the DHT to find other hosts
		routing,
		libp2p.NATPortMap(),
		libp2p.EnableNATService(),
	)

	var cfg config.Config
	if err := cfg.Apply(opts); err != nil {
		return nil, err
	}
	log.Debugf("libp2p node options: %+v\n", cfg)

	host, err := libp2p.New(ctx, opts)
	// host, err := libp2p.New(ctx,
	// 	libp2p.DefaultTransports,
	// 	// Attempt to open ports using uPNP for NATed hosts.
	// 	// Set the node private key
	// 	libp2p.Identity(*sk),
	// 	// Set the node listen addresses
	// 	libp2p.ListenAddrStrings(configNode.Addresses.Swarm...),
	// 	// Enable communication via relay nodes
	// 	libp2p.EnableRelay(),
	// 	// Let this host use relays and advertise itself on relays if
	// 	// it finds it is behind NAT. Use libp2p.Relay(options...) to
	// 	// enable active relays and more.
	// 	libp2p.EnableAutoRelay(),
	// 	//
	// 	libp2p.StaticRelays(relayNodes),
	// 	// Connect to a private network
	// 	libp2p.PrivateNetwork(psk),
	// 	// support TLS connections
	// 	libp2p.Security(libp2ptls.ID, libp2ptls.New),
	// 	// Let's prevent our peer from having too many
	// 	// connections by attaching a connection manager.
	// 	libp2p.ConnectionManager(connmgr.NewConnManager(
	// 		100,         // Lowwater
	// 		400,         // HighWater,
	// 		time.Minute, // GracePeriod
	// 	)),
	// 	// Let this host use the DHT to find other hosts
	// 	routing,
	// 	//libp2p.NATPortMap(),
	// 	//libp2p.EnableNATService(),
	// )

	if err != nil {
		log.Panic(err)
	}

	return &Node{
		ctx:            context.Background(),
		ID:             identity,
		Host:           host,
		ListenersLocal: newListenersLocal(),
		ListenersP2P:   newListenersP2P(host),

		Peerstore: host.Peerstore(),

		Streams: &StreamRegistry{
			Streams:     map[uint64]*Stream{},
			ConnManager: host.ConnManager(),
			conns:       map[peer.ID]int{},
		},
	}, nil
}

// CheckProtoExists checks whether a proto handler is registered to
// mux handler
func (node *Node) CheckProtoExists(proto string) bool {
	protos := node.Host.Mux().Protocols()

	for _, p := range protos {
		if p != proto {
			continue
		}
		return true
	}
	return false
}
