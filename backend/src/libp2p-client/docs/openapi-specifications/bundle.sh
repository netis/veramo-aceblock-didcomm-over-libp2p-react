#!/bin/bash

#
# Bundle OpenAPI using swagger-cli
# 
# https://github.com/APIDevTools/swagger-cli
# npm install -g @apidevtools/swagger-cli
#
# Oct. 2021

echo "Bundle openapi-ref.yaml into openapi.yaml"
swagger-cli bundle -o openapi.yaml -t yaml openapi-ref.yaml

yarn lint:openapi
