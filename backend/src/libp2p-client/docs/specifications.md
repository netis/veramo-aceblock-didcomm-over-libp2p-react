# Ace pear-to-pear specifications

- [Ace pear-to-pear specifications](#ace-pear-to-pear-specifications)
	- [Quickstart](#quickstart)
		- [Download the client](#download-the-client)
		- [Start the client](#start-the-client)
		- [Start listening for inbound connections](#start-listening-for-inbound-connections)
		- [Establish a connection with a 3rd party](#establish-a-connection-with-a-3rd-party)
		- [Examples](#examples)
	- [Architecture](#architecture)
		- [Roles](#roles)
			- [Client](#client)
			- [Bootnode](#bootnode)
			- [Relay Node](#relay-node)
			- [Message Pod](#message-pod)
			- [DID Registry](#did-registry)
	- [Protocol](#protocol)
	- [Security](#security)
		- [mTLS](#mtls)
		- [Signal Protocol](#signal-protocol)
	- [Data models](#data-models)
	- [Improvement proposals](#improvement-proposals)
	- [References](#references)

Ace pear-to-pear (Acep2p) builds on top of standards for decentralized identity
and peer-to-peer communication protocols (DID Documents, LibP2P), and
cryptographic protocols TLS and Signal Protocol (SP), which both support perfect
forward secrecy.
Uses are authenticated and end-to-end encrypted transport streams and
synchronous and asynchronous messaging.

## Quickstart

In this section, you will learn what are the minimal steps to start using the service.

### Download the client

You can download the client from TBD.

To build the client

- download the repository
- run `go build -o libp2p-client`

### Start the client

To start the client run
`./libp2p-client`

### Start listening for inbound connections

```bash
port=8899
endpoint="/libp2p/v1/listeners"
data='{"Protocol":"/x/didcomm/v2","TargetAddress":"/ip4/127.0.0.1/tcp/3002"}'

curl -X POST -d ${data} 127.0.0.1:${port}${endpoint}
```

### Establish a connection with a 3rd party

Exchange DID Documents
To get the minimal DID Document, visit `localhost:8899/libp2p/v1/properties/did-document`

To connect with another party, run

```bash
port=8899
endpoint="/libp2p/v1/listeners"

DIDDocument=<3rd party did-document goes here>
data='{"Protocol":"/x/didcomm/v2","ListenAddress":"/ip4/127.0.0.1/tcp/4445","DIDDocument":'${DIDDocument}''

curl -X POST -d ${data} 127.0.0.1:${port}${endpoint}
```

Now you can start the local service ...

### Examples 

- [SSH over libP2P](./examples/ssh-over-libp2p.md)
- [DIDComm over libP2P (coming soon)](./examples/didcomm-over-libp2p.md)

## Architecture

The ecosystem consists of the following roles

### Roles

#### Client

Is a service that can connect to the network, handle p2p streams and
connections with other clients and provide core communication capabilities.

#### Bootnode

Is a trusted peer in the network through which other nodes and clients learn
about other peers in the network.

#### Relay Node

Is a trusted peer in the network that can relay/proxy messages between two
clients that cannot establish a direct connection (e.g., both clients are behind
a NAT, firewall, etc.) As streams are end-to-end encrypted, relay nodes cannot
read the messages, but can learn about the connections.

#### Message Pod

Is a trusted service that can store messages for client(s) who are unavailable.

#### DID Registry

Is a service that supports registering, managing and resolving DIDs and DID
Documents.

## Protocol

Summary of the protocol

1. Exchange of DIDs via an authenticated channel
2. Resolve (or share) DID Documents and information about libp2p connection(s)
3. Connect to a libp2p network
4. Connect to a client
5. Perform DID-based mTLS authentication
6. Open a P2P stream

Use case A - Signal protocol messaging

1. Start a basic HTTP Signal Client
2. Perform x3dh key exchange
3. Exchange messages

Use case B - DIDComm messaging

## Security

### mTLS

### Signal Protocol

x3dh and double ratchet

## Data models

In this section we summarise data model of DID document

## Improvement proposals

- Failed connection handling, e.g., if an advertised relay node is not a relay node

## References

<https://spec.matrix.org/latest/>