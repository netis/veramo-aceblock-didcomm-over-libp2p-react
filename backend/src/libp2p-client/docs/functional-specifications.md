# Functional Specifications

LibP2P client specifications.

Table of content

- [Functional Specifications](#functional-specifications)
  - [Configuration and variables](#configuration-and-variables)
    - [LibP2P client configuration](#libp2p-client-configuration)
    - [LibP2P Network configuration](#libp2p-network-configuration)
  - [Command line arguments](#command-line-arguments)
  - [APIs](#apis)

## Configuration and variables

LibP2P client accepts two configurations

- Libp2p client configuration
- Network configuration

### LibP2P client configuration

Client configuration contains a subset of the [IPFS configurations](https://github.com/ipfs/go-ipfs/blob/master/docs/config.md).

| Claim            | Type   | Description                                                                              |
| ---------------- | ------ | ---------------------------------------------------------------------------------------- |
| Addresses        | object | [Definition](https://github.com/ipfs/go-ipfs/blob/master/docs/config.md#addresses)       |
| Addresses.Swarm  | list   | [Definition](https://github.com/ipfs/go-ipfs/blob/master/docs/config.md#addressesswarm)  |
| Identity         | object | [Definition](https://github.com/ipfs/go-ipfs/blob/master/docs/config.md#identity)        |
| Identity.PeerID  | string | [Definition](https://github.com/ipfs/go-ipfs/blob/master/docs/config.md#identitypeerid)  |
| Identity.PrivKey | string | [Definition](https://github.com/ipfs/go-ipfs/blob/master/docs/config.md#identityprivkey) |

### LibP2P Network configuration

The default configuration file `config.json` is in the `config` directory. The
configuration file contains information about:

| Claim          | Type   | Description                                                                        |
| -------------- | ------ | ---------------------------------------------------------------------------------- |
| LibP2PNetworks | list   | Contains information about the LibP2P networks.                                    |
| Name           | string | Name of the network.                                                               |
| Type           | string | Network type (open, permissioned)                                                  |
| Secret         | string | Network secret (permissioned networks only)                                        |
| Bootstrap      | list   | [Definition](https://github.com/ipfs/go-ipfs/blob/master/docs/config.md#bootstrap) |

## Command line arguments

| Argument      | Description                                                          |
| ------------- | -------------------------------------------------------------------- |
| -h            | Print help                                                           |
| -conf.client  | Path to the client configuration file                                |
| -conf.network | Path to the network configuration file                               |
| -listen       | Local listen address                                                 |
| -forward      | Local forward address                                                |
| -protocol     | Libp2p protocol name                                                 |
| -api          | REST API endpoint                                                    |
| -swarm        | Libp2p swarm address                                                 |
| -v            | Verbosity level: 1 - Error, 2 - Warn, 3 - Info, 4 - Debug, 5 - Trace |

Understanding the `-listen` and `-forward` commands

Example

> -listen /ip4/127.0.0.1/tcp/2000 -forward /ip4/127.0.0.1/tcp/3000

Send a message

message -> listenAddress -> local libp2p client -> remote libp2p client -> message receiver

Receive a message

libp2p client -> forwardAddress -> local service

## APIs

| Method | Endpoint                    | Description                    |
| ------ | --------------------------- | ------------------------------ |
| GET    | /libp2p/v1/connections      | List all active connections    |
| POST   | /libp2p/v1/connections      | Create a new connection        |
| GET    | /libp2p/v1/listeners        | Get a list of active listeners |
| POST   | /libp2p/v1/listeners        | Create a new connection        |
| GET    | /libp2p/v1/peers            | Get a list of connected peers  |
| GET    | /properties/v1/did          | Get node's DID                 |
| GET    | /properties/v1/did-document | Get node's DID Document        |

Endpoints are defined in an [OpenAPI specification](openapi-specifications/libp2p-client.yaml)
