# RFC - Security overview

- [RFC - Security overview](#rfc---security-overview)
	- [Transport security (synchronous messaging)](#transport-security-synchronous-messaging)
		- [Decentalized identifiers and TLS](#decentalized-identifiers-and-tls)
	- [Asynchronous messaging](#asynchronous-messaging)
		- [Signal protocol proposal v1](#signal-protocol-proposal-v1)
	- [References](#references)

This document summarises ideas for synchronous messaging (e.g., TLS, OTR, Noise) and
asynchronous messaging (e.g., Signal Protocol).

## Transport security (synchronous messaging)

In this section, we summarise ideas for transport secuirty.

### Decentalized identifiers and TLS

Libp2p supports TLS protected streams, where the certificates are derived from
the peer id public keys. The design enables to utilise all capabilities of TLS,
also perfect forward secrecy (PFS), mutual authentication.
Since libp2p enables to establish a direct stream between two clients, even if
it directed via a relay node (intermediary), such communication is encrypted end-to-end.

Decentralized identifiers (DIDs) and mutual TLS (mTLS) for establishing
end-to-end encrypted authenticated channels.
We propose to combine DIDs and mTLS to establish a P2P end-to-end encrypted
authenticated channel between two peers as follows:

1. Alice and Bob exchange their DIDs out-of-band
2. DIDs Document(s) contain information about the libp2p service, as presented in the service data model section
3. libp2p clients establish a connection directly (if one of the clients is
   publically available or if NAT travesal is possible) or via a relay node
4. mTLS handshake is performed; Note that the DID public key (peer ID) is transformed into a self-issued x509 certificate
5. Both clients verify the: self-issued TLS certificate(s), DIDs and DID Documents

Note: the mTLS handshake can be performed using any DID key.

In our examples, only TLS v1.3 is supported to enforce the perfect forward secrecy.

Service data model

| Claim                      | Type     | Format                                      | Description                                            |
| :------------------------- | :------- | :------------------------------------------ | :----------------------------------------------------- |
| Type                       | string   | \<network-name>                             | Official service name                                  |
| ServiceEndpoint            | string   | libp2p:\<network-id>:\<protocol>:\<peer-id> | Service Endpoint as string                             |
| ServiceEndpoint            | object   |                                             | Description                                            |
| ServiceEndpoint.Transport  | object   | libp2p                                      | MUST be "libp2p"                                       |
| ServiceEndpoint.Network    | string   | \<network-id>                               | Libp2p network name                                    |
| ServiceEndpoint.Protocol   | string   | \<protocol>                                 | Libp2p protocol                                        |
| ServiceEndpoint.PeerId     | string   | \<peer-id>                                  | Description                                            |
| ServiceEndpoint.RelayNodes | []string | List of \<peer-id>                          | List of allowed relay node peer IDs. ["*"] - allow any |

## Asynchronous messaging

### Signal protocol proposal v1

Simple "trivial" 1-to-1 signal messaging

1. Alice and Bob exchange DIDs out-of-band via an authenticated channel
2. Alice and Bob generate signal-protocol identities and exchange them
3. Alice and Bob generate signal-protocol key bundles and exchange them
4. Alice and Bob establish an authenticated p2p channel (using libp2p, DIDs). New endpoints are open
  GET /signal/v1/identifiers/{did}/bundles returns a new bundle
  POST /signal/v1/identifiers/{did}/messages send signal message

## References

- <https://signal.org/blog/asynchronous-security/>
- <https://en.wikipedia.org/wiki/Off-the-Record_Messaging>
- <https://en.wikipedia.org/wiki/Signal_Protocol>
- <https://otr.cypherpunks.ca/Protocol-v3-4.1.1.html>
- <https://gist.github.com/juniorz/9ae1d1f38fc24a2cb051>
- <https://github.com/coyim/otr3>
- <https://specs.status.im/spec/5#messaging>
- <https://github.com/status-im/status-go/tree/050dca833679fdda4c6cb3f07b48f50113822ca4/protocol>
- <https://github.com/Jamie-Matthews/Signal-Protocol-Demo/blob/master/SignalProtocolDemo.js>
