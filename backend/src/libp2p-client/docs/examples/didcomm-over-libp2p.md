# DIDComm over libP2P

We present, how DIDs, libp2p and DIDComm work together to establish a secure
messaging framework.

Setup:

- Alice and Bob create DIDs and DID Documents
- Alice and Bob exchange DIDs and DID Documents via an authenticated channel
