package main

import (
	"encoding/json"
	"io/fs"
	"io/ioutil"
	"log"
	"os"

	config "github.com/ipfs/go-ipfs-config"
	crypto "github.com/libp2p/go-libp2p-core/crypto"
	"github.com/libp2p/go-libp2p-core/peer"
	"github.com/sirupsen/logrus"
)

// Network configuration(s)
type ConfigNetwork struct {
	Name      string   `json:"Name,omitempty"`   // network name
	Type      string   `json:"Type"`             // network type
	Secret    string   `json:"Secret,omitempty"` // network secret
	Bootstrap []string `json:"Bootstrap"`        // list of bootstrap nodes
}

// Load network configuration file (local)
func (c *ConfigNetwork) Load(path string) error {

	// Set the default value if file doesn't exist
	_, err := os.Stat("config/networks.json")
	if os.IsNotExist(err) {
		return nil
	}
	// Read the local configuration file
	configData, err := ioutil.ReadFile(path)
	if err != nil {
		return err
	}

	// Unmarshal the configuration file
	err = json.Unmarshal(configData, c)
	if err != nil {
		return err
	}

	// TODO: check if deprecated
	// fmt.Println("[LoadBootstrap]", ipfsConfig)
	// bp, err := ipfsConfig.BootstrapPeers()
	// if err != nil {
	// 	return err
	// }
	// config := bootstrap.BootstrapConfigWithPeers(bp)
	// fmt.Println("[LoadBootstrap]", config.BootstrapPeers())

	return nil
}

// Node configuration
type ConfigNode struct {
	Addresses config.Addresses `json:"Addresses"`          // local node's addresses
	Identity  config.Identity  `json:"Identity,omitempty"` // local node's identity
	path      string
}

// Load local client configuration
func (c *ConfigNode) Load(path string) error {

	// Set the default value if file doesn't exist
	_, err := os.Stat("config/client.json")
	if os.IsNotExist(err) {
		c.path = "config/client.json"
		return nil
	}

	// Read the local configuration file
	configData, err := ioutil.ReadFile(path)
	if err != nil {
		return err
	}

	// Unmarshal the configuration file
	err = json.Unmarshal(configData, c)
	if err != nil {
		return err
	}

	// Set the configuration path
	c.path = path

	return nil
}

// getSecretKey returns a secret key if exists in the configuration file or creates and stores a new one
func (c *ConfigNode) getSecretKey() (*crypto.PrivKey, error) {

	// Try to load the secret key from the configuration file
	if c.Identity.PrivKey != "" {
		skBytes, err := crypto.ConfigDecodeKey(c.Identity.PrivKey)
		if err != nil {
			return nil, err
		}
		sk, err := crypto.UnmarshalPrivateKey(skBytes)
		if err != nil {
			return nil, err
		}
		return &sk, nil
	}

	// Create and store a new private key
	sk, _, err := crypto.GenerateKeyPair(
		crypto.Ed25519, // Select your key type. Ed25519 are nice short
		-1,             // Select key length when possible (i.e. RSA).
	)
	if err != nil {
		return nil, err
	}
	logrus.Debug("[getSecretKey] Created new secret key. Public key:", sk.GetPublic())

	// Store the private key and the node id
	identity, err := peer.IDFromPrivateKey(sk)
	if err != nil {
		panic(err)
	}
	c.Identity.PeerID = identity.Pretty()
	skBytes, err := crypto.MarshalPrivateKey(sk)
	if err != nil {
		panic(err)
	}
	c.Identity.PrivKey = crypto.ConfigEncodeKey(skBytes)

	// Store the config
	confBytes, err := json.Marshal(c)
	if err != nil {
		panic(err)
	}

	_, err = os.Stat("config")
	if os.IsNotExist(err) {
		if err := os.Mkdir("config", os.ModePerm); err != nil {
			log.Fatal(err)
		}
	}

	err = ioutil.WriteFile(c.path, confBytes, fs.FileMode(0600))
	if err != nil {
		panic(err)
	}

	return &sk, err

}
