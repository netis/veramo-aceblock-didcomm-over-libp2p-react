package main

import (
	"context"
	"encoding/base64"
	"fmt"
	"net/http"

	"github.com/go-kit/kit/endpoint"
	"github.com/hyperledger/aries-framework-go/pkg/doc/did"
)

// NewNodeDIDDocument
// Create node DID Document
func (n *Node) NewNodeDIDDocument() error {
	// Extract the node public key: key type: ed25519
	pk, err := n.ID.ExtractPublicKey()
	if err != nil {
		return err
	}
	// Convert public key to bytes
	pkBytes, err := pk.Raw()
	if err != nil {
		return err
	}

	pkBase64 := base64.StdEncoding.EncodeToString(pkBytes)

	routingKeys := []string{}
	for _, peerId := range n.Host.Peerstore().Peers() {
		routingKeys = append(routingKeys, peerId.String())
	}
	// Create a DID service
	service := did.Service{
		Type:            "DIDCommMessaging",
		RoutingKeys:     routingKeys,
		ServiceEndpoint: fmt.Sprintf("/ipfs/%s", n.ID.String()),
		Accept:          []string{"/x/didcomm/v2"},
	}

	// Construct DID Document options
	didOpts := did.WithService([]did.Service{service})
	didDoc := did.BuildDoc(didOpts)

	// Create DID
	didDoc.ID = fmt.Sprintf("did:x:%s", pkBase64)
	n.DIDDocument = didDoc

	return nil
}

func (n *Node) UpdateRoutingKeys() {
	routingKeys := []string{}
	for _, peerId := range n.Host.Peerstore().Peers() {
		routingKeys = append(routingKeys, peerId.String())
	}
	// Create a DID service
	service := did.Service{
		Type:            "DIDCommMessaging",
		RoutingKeys:     routingKeys,
		ServiceEndpoint: fmt.Sprintf("/ipfs/%s", n.ID.String()),
		Accept:          []string{"/x/didcomm/v2"},
	}
	n.DIDDocument.Service[0] = service
}

// NodeDIDDocumentRequest
// No input arguments
type NodeDIDDocumentRequest struct{}

// decodeNodeDIDDocumentRequest
func decodeNodeDIDDocumentRequest(_ context.Context, r *http.Request) (interface{}, error) {
	return NodeDIDDocumentRequest{}, nil
}

// NodeDIDDocumentEndpoint
func NodeDIDDocumentEndpoint(n *Node) endpoint.Endpoint {
	return func(_ context.Context, request interface{}) (interface{}, error) {
		return n.DIDDocument, nil
	}
}

// No input arguments
type NodeDIDRequest struct{}

// decodeNodeDIDRequest
func decodeNodeDIDRequest(_ context.Context, r *http.Request) (interface{}, error) {
	return NodeDIDRequest{}, nil
}

// NodeDIDEndpoint
func NodeDIDEndpoint(n *Node) endpoint.Endpoint {
	return func(_ context.Context, request interface{}) (interface{}, error) {
		return n.DIDDocument.ID, nil
	}
}

// NodeDIDDocumentServiceRequest
// No input arguments
type NodeDIDDocumentServiceRequest struct{}

// decodeNodeDIDDocumentRequest
func decodeNodeDIDDocumentServiceRequest(_ context.Context, r *http.Request) (interface{}, error) {
	return NodeDIDDocumentRequest{}, nil
}

// NodeDIDDocumentEndpoint
func NodeDIDDocumentServiceEndpoint(n *Node) endpoint.Endpoint {
	return func(_ context.Context, request interface{}) (interface{}, error) {
		return n.DIDDocument.Service, nil
	}
}
