package main

import (
	"context"
	"fmt"
	"time"

	log "github.com/sirupsen/logrus"

	tec "github.com/jbenet/go-temp-err-catcher"
	net "github.com/libp2p/go-libp2p-core/network"
	"github.com/libp2p/go-libp2p-core/peer"
	"github.com/libp2p/go-libp2p-core/protocol"
	ma "github.com/multiformats/go-multiaddr"
	manet "github.com/multiformats/go-multiaddr/net"
)

// localListener manet streams and proxies them to libp2p services
type localListener struct {
	ctx context.Context

	node *Node

	proto protocol.ID
	laddr ma.Multiaddr
	peer  peer.ID

	listener manet.Listener
}

// ForwardLocal creates new P2P stream to a remote listener
func (node *Node) ForwardLocal(ctx context.Context, peer peer.ID, proto protocol.ID, bindAddr ma.Multiaddr) (Listener, error) {
	listener := &localListener{
		ctx:   ctx,
		node:  node,
		proto: proto,
		peer:  peer,
	}

	maListener, err := manet.Listen(bindAddr)
	if err != nil {
		return nil, err
	}

	listener.listener = maListener
	listener.laddr = maListener.Multiaddr()

	if err := node.ListenersLocal.Register(listener); err != nil {
		return nil, err
	}

	go listener.acceptConns()

	return listener, nil
}

// dial dials a remote peer
func (l *localListener) dial(ctx context.Context) (net.Stream, error) {
	cctx, cancel := context.WithTimeout(ctx, time.Second*30) //TODO: configurable?
	defer cancel()

	return l.node.Host.NewStream(cctx, l.peer, l.proto)
}

// acceptConns accepts connections
func (l *localListener) acceptConns() {
	for {
		local, err := l.listener.Accept()
		if err != nil {
			if tec.ErrIsTemporary(err) {
				continue
			}
			return
		}

		log.Debugf("Accepted connection: %s", local.LocalAddr())
		go l.setupStream(local)
	}
}

// setupStream opens a remote stream
func (l *localListener) setupStream(local manet.Conn) {
	remote, err := l.dial(l.ctx)
	if err != nil {
		out := fmt.Sprintf("failed to dial to remote %s%s, reason: %v", l.peer.Pretty(), l.proto, err)
		log.Warnf("failed to dial to remote %s%s, reason: %v", l.peer.Pretty(), l.proto, err)
		local.Write([]byte(fmt.Sprintf("HTTP/1.1 500 Internal Server Error\nConnection: close\nContent-Length: %d\nContent-Type: text/html\n\n%s", len(out), out)))
		local.Close()
		return
	}

	stream := &Stream{
		Protocol: l.proto,

		OriginAddr: local.RemoteMultiaddr(),
		TargetAddr: l.TargetAddress(),
		peer:       l.peer,

		Local:  local,
		Remote: remote,

		Registry: l.node.Streams,
	}

	l.node.Streams.Register(stream)
}

// close closes the local listener
func (l *localListener) close() {
	l.listener.Close()
}

// Protocol returns the protocol ID
func (l *localListener) Protocol() protocol.ID {
	return l.proto
}

// ListenereAddress returns the multiaddress of the listener
func (l *localListener) ListenAddress() ma.Multiaddr {
	return l.laddr
}

// TargetAddress returns the multiaddress of the target
func (l *localListener) TargetAddress() ma.Multiaddr {
	addr, err := ma.NewMultiaddr(maPrefix + l.peer.Pretty())
	if err != nil {
		panic(err)
	}
	return addr
}

// key returns TBD
func (l *localListener) key() string {
	return l.ListenAddress().String()
}
