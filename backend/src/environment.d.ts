// Provide typings for process.env
declare global {
  namespace NodeJS {
    interface ProcessEnv {
      VERAMO_PORT: string;
      LIBP2P_API: string;
    }
  }
}

export {};