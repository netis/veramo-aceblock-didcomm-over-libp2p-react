import { DiffieHellman } from 'crypto';
import * as dotenv from 'dotenv';

dotenv.config();

export interface Config {
  veramoPort: number;
  didCommPort: number;
  libp2pApi: string;
}

export const loadConfig = (): Config => {
  let DIDCommPort = Math.round(Math.random() * 32000 + 2048);
    if(process.env.DID_COMM_PORT) {
      DIDCommPort = parseInt(process.env.DID_COMM_PORT);
    }
  if (process.env.VERAMO_PORT) {
    return {
      veramoPort: parseInt(process.env.VERAMO_PORT, 10),
      didCommPort: DIDCommPort,
      libp2pApi: process.env.LIBP2P_API || "localhost:10001",
    };
  }
  return {
    veramoPort: 10000,
    didCommPort: DIDCommPort,
    libp2pApi: process.env.LIBP2P_API || "localhost:10001",
  };
};
