import { agent, Addressbook, setSocket } from "./veramo/setup";
import { MessagingRouter, RequestWithAgentRouter } from "@veramo/remote-server";
import express, { Request, Response } from "express";
import { computeAddress } from "@ethersproject/transactions";
import bodyParser from "body-parser";
import http from "http";
import {
  IIdentifier,
} from "@veramo/core";
import { loadConfig } from "./configuration";
import { DIDCommMessagePacking } from "@veramo/did-comm";
import { Server } from "socket.io";

const { veramoPort, didCommPort, libp2pApi } = loadConfig();

// main
// Manages the backend services
async function main() {
  console.log("###########################################")
  console.log("# MetaVerse Chat V1.0 - backend service")
  console.log("# Version: 1.0")
  console.log("###########################################")
  console.log("[INFO] Starting the backend service")

  console.log(`[INFO] DIDComm endpoint port: ${didCommPort}`)

  // Start DIDComm router
  let didCommEndpointServer: http.Server;
  const requestWithAgent = RequestWithAgentRouter({ agent });
  // Set up the DIDComm messaging part
  await new Promise(async (resolve) => {

    //setup a server to receive HTTP messages and forward them to this agent to be processed as DIDComm messages
    const app = express();

    try {
      // Set up a local libp2p listener (listen on libp2p and forward to the local port)
      await createLocalListener(didCommPort);
      console.log(
        `[INFO][libp2p] Local listener registered at: localhost:${didCommPort}`
      );
    } catch (error) {
      // Error. Exit.
      console.log(`[ERROR] ${error}`);
      console.log("Exiting.");
      return;
    }

    // Create a local didcomm endpoint
    app.use(
      "/messaging",
      requestWithAgent,
      MessagingRouter({
        metaData: { type: "JWT", value: "integration test" },
      })
    );
    didCommEndpointServer = app.listen(didCommPort, () => {
      console.log(
        `[INFO] DIDComm service running at http://localhost:${didCommPort}/messaging`
      );
      resolve(true);
    });
  });
  console.log("[INFO] Setting up local endpoints");
  // API server
  const app = express();
  const jsonParser = bodyParser.json();

  // Backend Endpoints
  // Resolve DIDs
  app.get("/veramo/identifiers/v1/:did", getIdentifiersV1Did);
  // Create a new local user
  app.post("/veramo/messaging/v1/users", jsonParser, postMessagingV1Users);
  // Return local user info (DID Document)
  app.get("/veramo/messaging/v1/users/:alias", jsonParser, getUserByAlias);
  // Create a new libp2p service
  app.get("/libp2p/v1/services", jsonParser, GetLibp2pServices);
  // Establish a new connection
  app.post(
    "/veramo/messaging/v1/users/:sender_did/connections",
    jsonParser,
    postUsersV1Connections
  );
  // Send a message
  app.post(
    "/veramo/messaging/v1/users/:sender_did/receivers/:receiver_did/messages",
    jsonParser,
    postV1Messages
  );

  // Create an http server
  const httpServer = http
    .createServer(app);
  console.log("[INFO] HTTP server created");

  // Create a websocket service to push the inbound messages to the UI
  const io = new Server(httpServer, {});
  io.on("connection", (_socket: any) => {
    console.log("[DEBUG] Established websocket connection!!")
    setSocket(_socket);
    _socket.on("disconnect", (reason: any) => {
      console.log("[WARN] Lost connection with FE!", reason)
    })
  });
  console.log("[INFO] WebSocket server created");

  // Start the listener
  httpServer.listen(veramoPort, () =>
    console.log(`[INFO] API are available at http://localhost:${veramoPort}/`)
  );
}

// getIdentifiersV1Did
// Resolve a DID using the Veramo DID Resolver
const getIdentifiersV1Did = async (request: Request, response: Response) => {
  // Obtain the DID from the query params
  const did: string = request.params.did;
  try {
    // Resolve the DID
    const didResolutionResult = await agent.resolveDid({ didUrl: did });
    // Return the DID resolution result
    response.status(200).json(didResolutionResult);

  } catch (error) {
    console.log(`[ERROR] Failed to resolve the DID: ${did}. Error message: ${error}`);
    response.status(500);
    response.send(`Failed to resolve the DID: ${did}. Error message: ${error}`);
    return;
  }
};

// UserAlias
// Data object used to pass user alias information in the request body
export interface UserAlias {
  alias: string;
}

// libp2pService
// Response of the libp2p client that contains information about the libp2p services
export interface libp2pService {
  id: string;
  type: string;
  routingKeys: string[];
  serviceEndpoint: string;
  accept: string[];
}

// postMessagingV1Users 
// Create a new local user
const postMessagingV1Users = async (req: Request, response: Response) => {
  try {
    // Obtain user alias from the request body
    const alias: UserAlias = req.body;
    let identity: IIdentifier;

    try {
      // Check if user already exists
      identity = await agent.didManagerGetByAlias({ alias: alias.alias });
    } catch (error) {
      // User doesn't exist, create a new user
      identity = await agent.didManagerCreate({
        alias: alias.alias,
        kms: "local",
      });
    }

    // Get libp2p service info to update the service endpoint
    let services: libp2pService[];
    try {
      services = await getLibp2pServices();
    } catch (error) {
      console.log(`[ERROR] Failed to obtain libp2p services. Error message: ${error}`);
      response.status(500);
      response.send(`Failed to obtain libp2p services. Error message: ${error}`);
      return;
    }

    // TODO: At the moment we cannot publish the relay node info in an efficient way
    // For the purpose of the demo we assume clients connect to the same relay nodes
    for (let service of services) {
      try {
        await agent.didManagerAddService({
          did: identity.did,
          service: {
            id: "did:ethr:rinkeby:libp2p",
            type: "DIDCommMessaging",
            serviceEndpoint: service.serviceEndpoint,
            description: service.routingKeys.toString(),
          },
        });
      } catch (error) {
        console.log(`ERROR: ${error}`);
        response.status(400);
        const address = await computeAddress(
          "0x" + identity.keys[0].publicKeyHex
        );
        response.send(
          `Failed to update the DID service. Check if address ${address} has enough funds. Provider: ${identity.provider}`
        );
        return;
      }
    }
    // Return the identity
    response.status(200).json(identity);
  } catch (error) {
    console.log(error);
    response.status(400);
    response.send("Invalid request.");
    return;
  }
};

// getLibp2pServices
// Returns a the available libp2p service(s)
async function getLibp2pServices(): Promise<libp2pService[]> {
  // For now, consider the data is stored on a static `users.json` file
  let uri = `http://${libp2pApi}/libp2p/v1/properties/did-service`;
  console.log(uri);
  return (
    fetch(uri)
      // the JSON body is taken from the response
      .then((res) => res.json())
      .then((res) => {
        return res as libp2pService[];
      })
  );
}

// GetLibp2pServices
// libp2p services endpoint handler
const GetLibp2pServices = async (req: Request, response: Response) => {
  try {
    // Obtain the libp2p services
    const libp2pServices = await getLibp2pServices();
    // Return the list of libp2p services
    response.status(200).json(libp2pServices);
  } catch (error) {
    response.status(500);
    response.send(`Failed to obtain libp2p services. Error message ${error}`);
  }
};

// createLocalListener
// Creates a local libp2p listener
async function createLocalListener(targetPort: number): Promise<any> {
  console.log(`[INFO] Creating a local listener`);

  let uri = `http://${libp2pApi}/libp2p/v1/listeners`;
  const data = {
    Protocol: "/x/didcomm/v2",
    TargetAddress: `/ip4/127.0.0.1/tcp/${targetPort}`,
  };

  return (
    fetch(uri, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(data),
    })
      // the JSON body is taken from the response
      .then((res) => res.json())
      .then((res) => {
        console.log(`[INFO] Local listener created`);
        return res;
      })
  );
}

// Get DID Document by alias
// endpoint: /users/v1/{alias}
const getUserByAlias = async (req: Request, response: Response) => {
  try {
    // Obtain user alias
    const _alias: string = req.params.alias;
    console.log(_alias);
    const identity = await agent.didManagerGetByAlias({ alias: _alias });
    // Return the identity
    const didDoc = await agent.resolveDid({ didUrl: identity.did });
    response.status(200).json(didDoc);
  } catch (error) {
    response.status(400);
    response.send("Invalid request.");
  }
};

export interface libp2pConnectionRequest {
  Protocol: string; // Protocol name (in terms of libp2p); e.g., /x/didcomm/v2
  ListenAddress: string; // Local address to which we send the messages
  TargetAddress: string; // Remote libp2p address to which we forward the messages
}

export interface identity {
  did: string;
  alias: string;
}

export interface connectionsRequest {
  sender: identity;
  receiver: identity;
}

export interface connectionsResponse {
  did: string;
  connectionId: string;
  ephemeralDid: string;
}

// Connect with a peer via libp2p
// Endpoint: /users/v1/:alias/connections
// Header: "Content-Type: application/json"
// Body:
//   did: user did
//   alias: string; -- receiver
const postUsersV1Connections = async (req: Request, response: Response) => {
  try {
    const cr: connectionsRequest = req.body;
    const connectionId = cr.sender.did + "/" + cr.receiver.did;
    console.log(cr.sender.did, cr.receiver.did)
    let index: number;

    // 1.1 Check if we're already connected
    const connected = await Addressbook.get(connectionId);

    // 1.2 Check if DID is registered
    const didDocRes = await agent.resolveDid({ didUrl: cr.receiver.did });
    if (didDocRes.didDocument !== null) {
      if (didDocRes.didDocument.service !== undefined) {
        index = didDocRes.didDocument.service.length - 1;
      } else {
        response.status(400);
        console.log(didDocRes.didDocument.service);
        response.send("Service not defined");
        return;
      }
      // 1.3 Check if service is a valid libp2p service
      if (
        didDocRes.didDocument.service?.[index].serviceEndpoint !== undefined
      ) {
        if (
          !(didDocRes.didDocument.service?.[index].serviceEndpoint as string).startsWith(
            "/ipfs/"
          )
        ) {
          response.status(400);
          console.log("XX:", didDocRes.didDocument.service);
          response.send(
            `Invalid receiver service endpoint ${didDocRes.didDocument.service?.[
              didDocRes.didDocument.service.length - 1
            ].serviceEndpoint
            }. Service endpoint must begin with /ipfs/`
          );
          return;
        }
      } else {
        response.status(400);
        response.send("Receiver service endpoint is undefined.");
        return;
      }
    } else {
      response.status(400);
      console.log("[ERROR]: ", "Failed to resolve the reciever DID Document", didDocRes)
      response.send("Failed to resolve the receiver DID Document.");
      return;
    }

    let identity: IIdentifier;
    console.log("Connected:", connected);
    if (connected === undefined) {
      try {
        // Create a random local port
        const listeningPort = didCommPort;

        const lcr: libp2pConnectionRequest = {
          Protocol: "/x/didcomm/v2", // libp2p protocol definition
          ListenAddress: `/ip4/127.0.0.1/tcp/${listeningPort}`, // we're sending messages to this endpoint
          TargetAddress: didDocRes.didDocument.service?.[index].serviceEndpoint as string, // Remote libp2p address to which we're sending messages
        };

        console.log("LCR", lcr);

        // Establish a libp2p connection
        let uri = `http://${libp2pApi}/libp2p/v1/connections`;
        await fetch(uri, {
          method: "POST",
          headers: { "Content-Type": "application/json" },
          body: JSON.stringify(lcr),
        }).catch((error) => {
          console.log(error);
        });

        // Check if ephemeral DID already exists
        try {
          identity = await agent.didManagerGetByAlias({ alias: connectionId });
        } catch (error) {
          // Create an ephemeral did
          identity = await agent.didManagerCreate({ alias: connectionId });
        }
        console.log("Ephemeral id:", identity);

        // Store the connection information
        Addressbook.set(connectionId, {
          did: cr.receiver.did,
          port: listeningPort,
        });
      } catch (error) {
        response.status(400);
        console.log(error);
        response.send("Failed to resolve the receiver DID Document.");
        return;
      }
    } else {
      identity = await agent.didManagerGetByAlias({ alias: connectionId });
    }
    // // 2. return an unsigned message
    // const msg: connectionsResponse = {
    //   did: cr.sender.did,
    //   connectionId: connectionId,
    //   ephemeralDid: (await agent.didManagerGetByAlias({ alias: connectionId }))
    //     .did,
    // };

    // 3. Send a "hello world" message
    const msg: Message = {
      type: "DIDComm",
      id: "0",
      body: {
        iss: cr.sender.did,
        aud: cr.receiver.did,
        ephemeralDid: identity.did,
      },
      packing: "jws"
    };

    // Try to send a message
    console.log("[INFO] Sending a message")
    await sendMessage(cr.sender.did, cr.receiver.did, msg);
    console.log("[INFO] Message sent")

    response.status(200);
    response.send("Success");
  } catch (error) {
    console.log(`[ERROR] Error message: ${error}`);
    response.status(400);
    response.send(`${error}`);
    return;
  }
};

export interface Message {
  type: string;
  id: string;
  body: object;
  packing: string;
}

// Send message sends a DIDComm Message
const sendMessage = async (
  sender: string,
  receiver: string,
  message: Message,
) => {
  try {
    // Construct the connection id
    const connectionId = sender + "/" + receiver;

    // Obtain the user info (ui)
    const ui = await Addressbook.get(connectionId);

    // Obtain the "connection" DID by alias (== connectionId)
    const connectionDid = await agent.didManagerGetByAlias({
      alias: connectionId,
    });

    // Pack the message
    // Note: Currently we support only JWS
    const packedMessage = await agent.packDIDCommMessage({
      packing: <DIDCommMessagePacking>message.packing,
      message: {
        from: connectionDid.did,
        to: receiver, // TODO: We need to obtain the remote connectionDid
        type: "JWT",
        id: message.id,
        body: message.body,
      },
    });

    // Send a message
    const uri = `http://localhost:${ui.port}/messaging`;
    console.log(`[INFO] Sending message to ${uri}`);
    const response = await fetch(uri, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: packedMessage.message,
    });
    let resp: string = await response.text();
    if(response.status !== 200) {
      // something went wrong
      throw resp;
    }
    console.log(`[INFO] Message sent. ${resp}\n${response.status}`);
    return;
  } catch (error) {
    throw error;
  }
};

// postUsersV1Messages handles the outbound messages
const postV1Messages = async (request: Request, response: Response) => {
  try {
    // Obtain sender and receiver did from URL params
    const senderDid: string = request.params.sender_did;
    const receiverDid: string = request.params.receiver_did;
    // Obtain the message from the request body
    const message: Message = request.body;

    console.log(senderDid, receiverDid, message)

    // Send a message
    await sendMessage(senderDid, receiverDid, message);

    response.status(200);
    response.send("Success");
  } catch (error) {
    console.log(`[ERROR] ${error}`)
    response.status(400);
    response.send("Invalid request.");
  }
};

// Run the main function
main().catch(console.log);