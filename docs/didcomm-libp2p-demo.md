# DIDComm messaging through libp2p in action

Alen Horvat (AceBlock)

- [DIDComm messaging through libp2p in action](#didcomm-messaging-through-libp2p-in-action)
	- [Components and secret keys](#components-and-secret-keys)
	- [Messaging](#messaging)
	- [Creating and registering DIDs](#creating-and-registering-dids)
	- [Establishing a connection](#establishing-a-connection)
	- [DIDComm Messaging](#didcomm-messaging)
	- [Use cases](#use-cases)
	- [Outlook](#outlook)
- [Appendix](#appendix)
	- [Sequence diagram](#sequence-diagram)
	- [Architecture](#architecture)
		- [Wallet setup](#wallet-setup)
		- [DID creation and registration](#did-creation-and-registration)
		- [Out-of-band DID exchange](#out-of-band-did-exchange)
		- [Connecting to a libp2p network](#connecting-to-a-libp2p-network)
		- [Establishing a session](#establishing-a-session)

In our previous work [DIDComm messaging through
libp2p](https://medium.com/uport/didcomm-messaging-through-libp2p-cffe0f06a062)
we outlined a decentralised messaging framework built
on decentralised identifiers (DIDs), Verifiable Credentials, DID communication (DIDComm) and libp2p.
In this article, we present a simple decentralised P2P
[chat](https://bitbucket.org/netis/veramo-aceblock-didcomm-over-libp2p-react/src/master/)
built with the Veramo framework (DID management,DIDComm), the AceBlock libp2p
client and IPFS as relay nodes.

Alice and Bob want to exchange messages privately and securely, without a
central party. Alice and Bob can achieve this as follows

- they create and register their DIDs in a DID Registry
- they exchange their DIDs out-of band
- they establish an authenticated and encrypted P2P connection via a libp2p overlay network
- they exchange messages using DIDComm messaging

The out-of-band DID exchange is a one-time action. After that, Alice and Bob can
rotate their keys without notifying each other.

## Components and secret keys

In the diagram below, we depict the main components of the system and their
relations. Note that the diagram reflects the design of the demo app to showcase
the use of different secret keys.

![component-diagram](diagrams/components.png)

Note that we have three types of secret keys we use for different purposes. DID
keys, libp2p keys and DIDComm keys.

The DID keys are managed by a Web3 Wallet and are used for user authentication.
Users can rotate the keys any time.
Libp2p secret keys are managed by the libp2p client. The public key or the peer
ID is used for discovery, client authentication and key agreement. Peer ID is
public and is stored in a DID Document of the user. In the demo, the libp2p
secret keys are not rotated. However, a time-based or session-based
key-rotation scheme can be introduced to prevent connection tracing.
We also introduce DIDComm session keys. They are used for DIDComm messaging and
are rotated at every session (they can be also rotated during the session). The
session keys are exchanged right after the two libp2p clients establish a
connection.

## Messaging

Alice and Bob need to perform the following steps to authenticate and establish
a first connection:

Set-up phase:

1. Set up a Web3 Wallet (MetaMask)
2. Create a DID
3. Register the DID
4. Update the DID Document with the peer ID
5. Exchange the DIDs out-of-band

Messaging phase:

6. Establish and accept the incomming connection
7. Exchange messages

The set-up phase is carried out only once; most of the steps are executed behind
the scene.

The demo uses the following Web3 and Self-sovereign identity (SSI) components:

- Web3 Wallet MetaMask\* for managing DIDs and user authentication
- DIDs, did:ethr method, for peer discovery (Veramo)
- DIDComm Messaging for message packing (Veramo)
- LibP2P for establishing an authenticated and end-to-end encrypted P2P communication channel (AceBlock)
- LibP2P relay nodes (IPFS)

\* One can integrate any Web3 wallet.

Below we present a high-level component diagram.
![component-diagram](diagrams/AceSpace%20component%20diagram.png)

Alice, in her environment, runs a MetaVerse Chat client that consists of a user
interface and backend services. The backend services are built with Veramo and
AceBlock solutions. Veramo handles all the business operations (DID management,
DIDComm), whereas the AceBlock libp2p client manages the connections. Alice also
needs a Web3 wallet. In this demo, we are using MetaMask.

## Creating and registering DIDs

DIDs and DID Documents serve only for service discovery purposes. Alice's DID Document holds the following information

- Ethereum address
- libp2p client peerID (public key)
- libp2p network name
- libp2p protocol name

In the demo, we assume we connect through a single network and we use the
'/x/didcomm/v2' the libp2p protocol name. Creating and registering a DID
(method: did:ethr) is a very simple process, thanks to Veramo.

After Alice and Bob create their DIDs, they exchange them out-of-band.

## Establishing a connection

Several steps are performed before the communication stream is established.
First, Alice provides Bob's DID to the client to resolve the DID and
obtain the corresponding DID Document. The DID Document contains all the
necessary information to establish a connection.
The client extracts Bob's ethereum address, libp2p client peerID, network name
and the protocol name.

Second, the libp2p client connects to the bootstrap nodes of the overlay libp2p
network. In case Bob's client is publically reachable (has public IP and open
ports), connecting to other peers is not required.

Once Alice's client discovers Bob's client, the two clients perform a mutual DID-TLS
authentication. Mutual DID-TLS authentication is the same as mTLS, except that the
x509 certificates are derived from the DID keys (in our case peerIDs in the DID
Document) and the root of trust originates from the DID Registry. Note that any
other mutual authentication protocol can be plugged in. Upon successful
authentication, Bob is asked to accept or refuse the connection request.

Now an authenticated and end-to-end encrypted communication stream is
established. The demo implements TLS v1.3 (only algorithms with
perfect-forward-secrecy).

Note: The current libp2p implementation supports TLS and Noise Protocol.

## DIDComm Messaging

Now Alice and Bob can exchange DIDComm messages. We use the Veramo framework to
create, manage, exchange and verify the DIDComm messages.

Note that we also introduce an additional set of DIDs - ephemeral or session
DIDs. These DIDs (and DID keys) are created and managed locally and are only
shared with the party we communicate with. In our demo, Alice and Bob both
create session DIDs and they exchange the corresponding DID Documents in the
very first message exchange. Messages are signed (and/or encrypted) using keys
registered in the public DID Document. All the subsequent DIDComm messages are
created and verified using the session DID keys.

## Use cases

The messaging framework we outlined is open, decentralized and applicable to
both public and private or enterprise solutions. As it is a low-level framework
it can be used for a wide range of applications and can also serve as a
framework for cross-app communication.

We also demonstrated the use of libp2p (for IoT p2p communication), blockchain
(for decentralized access management and autonomous data replication) and SSI for
an identity layer in one of the most promising e-mobility use case (Gen-I)[].
The use case involves SSI for IoT devices, enterprises and end-users.

## Outlook

In the future we'll present the async flow ....

Stay tuned for more updates and contact us if you want to get involved!

# Appendix

## Sequence diagram

![sequence-diagram2](diagrams/high-level-sequence-diagram.png)

## Architecture

In the sections that follow, we summarise the architecture of the demo.

The demo consists of the following components

- a simple user interface
- backend services for DID management and DIDComm messaging (using Veramo)
- customised libp2p client

Sequence of actions is as follows:

- Connecting a Web3 Wallet
- DID creation and registration
- Update the DID Document services (DIDComm)
- Out-of-band DID exchange
- Establishing connection
  - Resolve the DID
  - Connect with a remote libp2p client
  - Mutual authenticatoin
  - Create "session" DID
  - DIDComm message exchange using ephemeral DIDs

### Wallet setup

Wallet is used to create and manage private keys that are used for creation,
registration and management of DIDs. did:ethr DID
method is supported.

Users set up a hardware or software (web3) wallet. Currently only wallets
supported by Metamask can be used.

Note that the protocol itself is not limited to MetaMask and integration with
other wallets can be supported.

### DID creation and registration

DIDs are managed using the Veramo library. Once DID is created, we need to
update the service endpoints so other users can discover us using our DID.
To update the DID service property, we are prompted to sign the blockchain
transaction using our wallet.

### Out-of-band DID exchange

Once we update our DIDs, we can exchange them out of band with other parties.

### Connecting to a libp2p network

- Resolve the DID Document
- Extract the service information
- Connect to a libp2p network
- Connect with a peer
- Perform a mutual authentication (DID-TLS)
- Establish a connection

### Establishing a session

- Create ephemeral DIDs for the session.
- Exchange the DID (public keys) in the first message exchange
- Exchange DID messages.
